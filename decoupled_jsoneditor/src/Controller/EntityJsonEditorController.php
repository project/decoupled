<?php

namespace Drupal\decoupled_jsoneditor\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;

/**
 * Controller for decoupled entity.
 *
 * @see \Drupal\decoupled_jsoneditor\Routing\RouteSubscriber
 * @see \Drupal\decoupled_jsoneditor\Plugin\Derivative\DecoupledLocalTasks
 */
class EntityJsonEditorController extends ControllerBase {

  /**
   * Displays the current entity in a Json Editor.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function jsonapi() {
//    $entity = $this->getEntityFromRouteMatch();
    $entity = self::getEntityFromRouteMatch();
    $options = [
      'method' => $entity->getEntityTypeId() . '/' . $entity->bundle() . '/' .$entity->uuid(),
      'param' => '',
      'verify' => FALSE,
    ];

    $values = [];
    if(\Drupal::moduleHandler()->moduleExists('xtcguzzle')){
      $handler = XtcLoaderHandler::getHandlerFromProfile('decoupled', $options);
      if (!empty($handler)) {
        $values = Json::decode($handler->processContent());
      }
    }

    return [
      '#theme' => 'json_editor',
      '#attached' => [
        'library' => 'json_editor/json-editor',
        'drupalSettings' => [
          'json_editor' => $values,
        ],
      ],
    ];
  }

  /**
   * Returns the loaded structure of the current entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function getEntityFromRouteMatch() {
    $route_match = \Drupal::routeMatch();
    $entity_type_id = $route_match->getRouteObject()->compile()->getVariables()[0];
    $eid = $route_match->getParameter($entity_type_id);
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    return $storage->load($eid);
  }

}
