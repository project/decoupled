<?php

namespace Drupal\decoupled_jsoneditor\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local task definitions for all entity bundles.
 *
 * @see \Drupal\decoupled_jsoneditor\Controller\EntityDebugController
 * @see \Drupal\decoupled_jsoneditor\Routing\RouteSubscriber
 */
class DecoupledLocalTasks extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
      $this->derivatives["decoupled.$entity_type_id.json_editor"] = [
        'route_name' => "decoupled.$entity_type_id.json_editor",
        'base_route' => "entity.$entity_type_id.canonical",
        'weight' => 100,
        'title' => $this->t('{Json:API}'),
      ];
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
