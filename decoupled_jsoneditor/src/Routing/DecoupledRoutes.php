<?php


namespace Drupal\decoupled_jsoneditor\Routing;


use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class DecoupledRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $route_collection = new RouteCollection();
    foreach (\Drupal::entityTypeManager()
               ->getDefinitions() as $entity_type_id => $entity_type) {
      $route = new Route(
      // Path to attach this route to:
        '/decoupled/jsoneditor/' . $entity_type_id . '/{' . $entity_type_id . '}',
        // Route defaults:
        [
          '_controller' => '\Drupal\decoupled_jsoneditor\Controller\EntityJsonEditorController::jsonapi',
          '_title' => '{Json:API}',
        ],
        // Route requirements:
        [
          '_permission' => 'access content',
        ]
      );
      // Add the route under the name 'example.content'.
      $route_collection->add("decoupled.$entity_type_id.json_editor", $route);
    }
    return $route_collection;
  }

}
